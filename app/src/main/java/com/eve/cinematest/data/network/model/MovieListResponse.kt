package com.eve.cinematest.data.network.model

data class MovieListResponse(
    val page: Int,
    val total_pages: Int,
    val total_results: Int,
    val results: List<MovieListItem>
) {


}