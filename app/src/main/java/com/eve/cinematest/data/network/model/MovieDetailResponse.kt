package com.eve.cinematest.data.network.model

data class MovieDetailResponse(
    val backdrop_path: String,
    val popularity: Float,
    val original_title: String,
    val overview: String,
    val original_language: String,
    val runtime: Int,
    val spoken_languages: List<LanguageData>,
    val genres: List<GenresData>
) {
    inner class LanguageData(val english_name: String)
    inner class GenresData(val name: String)
}
