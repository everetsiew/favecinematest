package com.eve.cinematest.data.network

import com.eve.cinematest.data.network.model.MovieDetailResponse
import com.eve.cinematest.data.network.model.MovieListResponse
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NetworkHelper {
    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkInterceptor
        ): NetworkHelper {
            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okkHttpclient)
                .baseUrl("http://api.themoviedb.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(NetworkHelper::class.java)
        }
    }

    @GET("/3/discover/movie")
    suspend fun getMovieList(
        @Query("api_key") api_key: String,
        @Query("sort_by") sort_by: String,
        @Query("page") page: Int
    ): Response<MovieListResponse>

    @GET("/3/movie/{id}")
    suspend fun getMovieDetail(
        @Path("id") name: String,
        @Query("api_key") api_key: String
    ): Response<MovieDetailResponse>

}