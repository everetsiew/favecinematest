package com.eve.cinematest.data.repo

import com.eve.cinematest.data.network.NetworkHelper
import com.eve.cinematest.data.network.NetworkResponse
import com.eve.cinematest.data.network.model.MovieDetailResponse
import com.eve.cinematest.data.network.model.MovieListResponse


class MovieRepo(private val api: NetworkHelper) : NetworkResponse() {

    suspend fun getMovieList(page: Int, sort: String): MovieListResponse {
        return onCallApi {
            api.getMovieList(
                "328c283cd27bd1877d9080ccb1604c91",
                sort,
                page
            )
        }
    }

    suspend fun getMovieDetail(id: String): MovieDetailResponse {
        return onCallApi {
            api.getMovieDetail(
                id,
                "328c283cd27bd1877d9080ccb1604c91"
            )
        }
    }

}