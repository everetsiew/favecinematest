package com.eve.cinematest.data.network.model

data class MovieListItem(
    val id: Int,
    val original_title: String,
    val poster_path: String,
    val popularity: Float,
    val vote_average: Float
)