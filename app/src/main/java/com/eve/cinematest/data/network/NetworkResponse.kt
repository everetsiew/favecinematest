package com.eve.cinematest.data.network

import retrofit2.Response

abstract class NetworkResponse {

    suspend fun <T : Any> onCallApi(call: suspend () -> Response<T>): T {
        val response = call.invoke()
        if (response.isSuccessful) {
            return response.body()!!
        } else {
            throw Exception("error")
        }
    }
}