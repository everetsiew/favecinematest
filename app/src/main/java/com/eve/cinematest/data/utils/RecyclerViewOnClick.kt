package com.eve.pokedex.data.utils

import android.view.View

interface RecyclerViewOnClick {
    fun onRecyclerViewClick(view: View, data: Any)
    fun loadingListData(count: Int, position: Int)
}