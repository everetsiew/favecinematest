package com.eve.cinematest.view.detail.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.eve.cinematest.data.repo.MovieRepo

class DetailViewModelFactory(private val repo: MovieRepo) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DetailViewModel(repo) as T
    }
}