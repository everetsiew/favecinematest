package com.eve.cinematest.view.detail.viewModel

import androidx.lifecycle.ViewModel
import com.eve.cinematest.data.network.model.MovieDetailResponse
import com.eve.cinematest.data.repo.MovieRepo
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.io.IOException

@ExperimentalCoroutinesApi
class DetailViewModel(private val repo: MovieRepo) : ViewModel() {
    private lateinit var job: Job

    private val _movieDetailState = MutableStateFlow<MovieDetailState>(MovieDetailState.IDLE)
    val movieDetailState: StateFlow<MovieDetailState> = _movieDetailState

    sealed class MovieDetailState {
        data class SUCCESS(val movieDetail: MovieDetailResponse) : MovieDetailState()
        object FAILED : MovieDetailState()
        object LOADING : MovieDetailState()
        object IDLE : MovieDetailState()
    }

    fun getMovieDetail(id: String) {
        _movieDetailState.value = MovieDetailState.LOADING

        job = CoroutineScope(Dispatchers.IO).launch {
            try {
                val movieDetail = repo.getMovieDetail(id)
                _movieDetailState.value = MovieDetailState.SUCCESS(movieDetail)
            } catch (e: IOException) {
                _movieDetailState.value = MovieDetailState.FAILED
                e.printStackTrace()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}