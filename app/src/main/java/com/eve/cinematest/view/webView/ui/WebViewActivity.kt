package com.eve.cinematest.view.webView.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.eve.cinematest.R
import com.eve.cinematest.databinding.ActivityWebviewBinding
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein

class WebViewActivity : AppCompatActivity(), KodeinAware {
    override val kodein by kodein()
    private lateinit var binding: ActivityWebviewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_webview)

        val url = intent.getStringExtra("url")!!
        binding.webView.loadUrl(url)
    }
}