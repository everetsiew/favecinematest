package com.eve.cinematest.view.main.ui

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.eve.cinematest.R
import com.eve.cinematest.data.network.model.MovieListItem
import com.eve.cinematest.databinding.ActivityMainBinding
import com.eve.cinematest.view.detail.ui.DetailActivity
import com.eve.cinematest.view.main.adapter.MovieAdapter
import com.eve.cinematest.view.main.viewModel.MainViewModel
import com.eve.cinematest.view.main.viewModel.MainViewModelFactory
import com.eve.pokedex.data.utils.RecyclerViewOnClick
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

@ExperimentalCoroutinesApi
class MainActivity : AppCompatActivity(), KodeinAware, RecyclerViewOnClick {
    override val kodein by kodein()
    private val factory: MainViewModelFactory by instance()
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private var sortType: String = "release_date.desc"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this, factory).get(MainViewModel::class.java)

        viewModel.getMovieList(sortType, true)
        lifecycleScope.launchWhenCreated {
            viewModel.movieListState.collect {
                when (it) {
                    is MainViewModel.MovieListState.SUCCESS -> {
                        if (binding.recyclerView.adapter == null) {
                            binding.recyclerView.also { recyclerView ->
                                recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
                                recyclerView.adapter =
                                    MovieAdapter(this@MainActivity, it.movieList, this@MainActivity)
                            }
                        } else {
                            (binding.recyclerView.adapter as MovieAdapter).addDataList(it.movieList)
                        }
                        binding.swipeContainer.isRefreshing = false
                    }
                    is MainViewModel.MovieListState.FAILED -> {
                        binding.swipeContainer.isRefreshing = false
                    }
                    is MainViewModel.MovieListState.LOADING -> {
                        binding.swipeContainer.isRefreshing = false
                    }
                    is MainViewModel.MovieListState.IDLE -> {

                    }
                }

            }
        }
        binding.swipeContainer.setOnRefreshListener {
            (binding.recyclerView.adapter as MovieAdapter).clearData()
            viewModel.getMovieList(sortType, true)
        }
        binding.sortDate.setOnClickListener {
            if (sortType == "release_date.desc")
                return@setOnClickListener
            sortType = "release_date.desc"
            (binding.recyclerView.adapter as MovieAdapter).clearData()
            viewModel.getMovieList(sortType, true)
            binding.sortDate.setBackgroundColor(Color.parseColor("#000000"))
            binding.sortAlpha.setBackgroundColor(Color.parseColor("#555555"))
            binding.sortRate.setBackgroundColor(Color.parseColor("#555555"))
        }
        binding.sortAlpha.setOnClickListener {
            if (sortType == "original_title.asc")
                return@setOnClickListener
            sortType = "original_title.asc"
            (binding.recyclerView.adapter as MovieAdapter).clearData()
            viewModel.getMovieList(sortType, true)
            binding.sortDate.setBackgroundColor(Color.parseColor("#555555"))
            binding.sortAlpha.setBackgroundColor(Color.parseColor("#000000"))
            binding.sortRate.setBackgroundColor(Color.parseColor("#555555"))
        }
        binding.sortRate.setOnClickListener {
            if (sortType == "popularity.desc")
                return@setOnClickListener
            sortType = "popularity.desc"
            (binding.recyclerView.adapter as MovieAdapter).clearData()
            viewModel.getMovieList(sortType, true)
            binding.sortDate.setBackgroundColor(Color.parseColor("#555555"))
            binding.sortAlpha.setBackgroundColor(Color.parseColor("#555555"))
            binding.sortRate.setBackgroundColor(Color.parseColor("#000000"))
        }
    }

    override fun onRecyclerViewClick(view: View, data: Any) {
        data as MovieListItem
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("id", data.id.toString())
        startActivity(intent)
    }

    override fun loadingListData(count: Int, position: Int) {
        if (position >= count - 1)
            viewModel.getMovieList(sortType, false)
    }
}