package com.eve.cinematest.view.detail.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.eve.cinematest.R
import com.eve.cinematest.databinding.ActivityDetailBinding
import com.eve.cinematest.view.detail.viewModel.DetailViewModel
import com.eve.cinematest.view.detail.viewModel.DetailViewModelFactory
import com.eve.cinematest.view.webView.ui.WebViewActivity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

@ExperimentalCoroutinesApi
class DetailActivity : AppCompatActivity(), KodeinAware {
    override val kodein by kodein()
    private val factory: DetailViewModelFactory by instance()
    private lateinit var binding: ActivityDetailBinding
    private lateinit var viewModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        viewModel = ViewModelProvider(this, factory).get(DetailViewModel::class.java)

        viewModel.getMovieDetail(intent.getStringExtra("id")!!)

        lifecycleScope.launchWhenCreated {
            viewModel.movieDetailState.collect { it ->
                when (it) {
                    is DetailViewModel.MovieDetailState.SUCCESS -> {
                        binding.movie = it.movieDetail

                        Glide.with(this@DetailActivity)
                            .load("https://image.tmdb.org/t/p/w500/" + it.movieDetail.backdrop_path)
                            .into(binding.backDrop)

                        val genreBuilder = StringBuilder()
                        it.movieDetail.genres.forEach {
                            genreBuilder.append("• ".plus(it.name.plus("\n")))
                        }
                        binding.genres.text = genreBuilder.toString()

                        val languageBuilder = StringBuilder()
                        it.movieDetail.spoken_languages.forEach {
                            languageBuilder.append("• ".plus(it.english_name.plus("\n")))
                        }
                        binding.language.text = languageBuilder.toString()
                    }
                }
            }
        }

        binding.bookMovie.setOnClickListener {
            val intent = Intent(this, WebViewActivity::class.java)
            intent.putExtra("url", "https://www.cathaycineplexes.com.sg")
            startActivity(intent)
        }
    }

}