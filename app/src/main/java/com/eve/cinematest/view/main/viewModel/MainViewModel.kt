package com.eve.cinematest.view.main.viewModel

import android.util.Log
import androidx.lifecycle.ViewModel
import com.eve.cinematest.data.network.model.MovieListItem
import com.eve.cinematest.data.repo.MovieRepo
import com.google.gson.Gson
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.io.IOException

@ExperimentalCoroutinesApi
class MainViewModel(private val repo: MovieRepo) : ViewModel() {
    private lateinit var job: Job
    private val _movieListState = MutableStateFlow<MovieListState>(MovieListState.IDLE)
    val movieListState: StateFlow<MovieListState> = _movieListState
    private var page = 0

    sealed class MovieListState {
        data class SUCCESS(val movieList: MutableList<MovieListItem>) : MovieListState()
        object FAILED : MovieListState()
        object LOADING : MovieListState()
        object IDLE : MovieListState()
    }

    fun getMovieList(sort: String, clear: Boolean) {
        if (_movieListState.value == MovieListState.LOADING)
            return
        _movieListState.value = MovieListState.LOADING
        if (clear)
            page = 0
        page++
        job = CoroutineScope(Dispatchers.IO).launch {
            try {
                val movieList = repo.getMovieList(page, sort).results
                _movieListState.value = MovieListState.SUCCESS(movieList.toMutableList())
            } catch (e: IOException) {
                _movieListState.value = MovieListState.FAILED
                e.printStackTrace()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }

}