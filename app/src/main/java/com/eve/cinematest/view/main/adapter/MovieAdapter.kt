package com.eve.cinematest.view.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eve.cinematest.R
import com.eve.cinematest.data.network.model.MovieListItem
import com.eve.cinematest.databinding.RecyclerviewMovieBinding
import com.eve.pokedex.data.utils.RecyclerViewOnClick

class MovieAdapter(
    private val context: Context,
    private val movieList: MutableList<MovieListItem>,
    private val clickListener: RecyclerViewOnClick
) :
    RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    inner class ViewHolder(val recyclerViewMovieBinding: RecyclerviewMovieBinding) :
        RecyclerView.ViewHolder(recyclerViewMovieBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.recyclerview_movie, parent, false
            )
        )

    fun addDataList(dataList: List<MovieListItem>) {
        movieList.addAll(dataList)
        notifyDataSetChanged()
    }

    fun clearData() {
        movieList.clear()
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.recyclerViewMovieBinding.movie = movieList[position]

        Glide.with(context)
            .load("https://image.tmdb.org/t/p/w500/" + movieList[position].poster_path)
            .into(holder.recyclerViewMovieBinding.poster)
        holder.recyclerViewMovieBinding.root.setOnClickListener {
            clickListener.onRecyclerViewClick(
                holder.recyclerViewMovieBinding.root,
                movieList[position]
            )
        }
        clickListener.loadingListData(movieList.size, position)

    }

    override fun getItemCount() = movieList.size
}