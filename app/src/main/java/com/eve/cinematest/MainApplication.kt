package com.eve.cinematest

import android.app.Application
import com.eve.cinematest.data.network.NetworkHelper
import com.eve.cinematest.data.network.NetworkInterceptor
import com.eve.cinematest.data.repo.MovieRepo
import com.eve.cinematest.view.detail.viewModel.DetailViewModelFactory
import com.eve.cinematest.view.main.viewModel.MainViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MainApplication : Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@MainApplication))

        bind() from singleton { NetworkInterceptor(instance()) }
        bind() from singleton { NetworkHelper(instance()) }
        bind() from singleton { MovieRepo(instance()) }
        bind() from provider { MainViewModelFactory(instance()) }
        bind() from provider { DetailViewModelFactory(instance()) }
    }
}